package com.example.canvastutorial;

import android.graphics.Bitmap;

public class Contact {

    String name ;
    Bitmap bitmap ;

    Contact(String name, Bitmap bitmap) {
        this.name = name ;
        this.bitmap = bitmap;
    }
}

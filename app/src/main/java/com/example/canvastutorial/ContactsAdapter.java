package com.example.canvastutorial;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ContactsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Contact> contactList = new ArrayList<>();

    public void addItem(Contact newItem) {
        this.contactList.add(0, newItem);
        notifyDataSetChanged();
    }

    public void removeLastItem() {
        if (contactList.size() > 0) {
            contactList.remove(0);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ContactViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((ContactViewHolder) holder).setData(contactList.get(position));
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }
}

class ContactViewHolder extends RecyclerView.ViewHolder {

    TextView contactName ;

    public ContactViewHolder(@NonNull View itemView) {
        super(itemView);

        contactName = itemView.findViewById(R.id.contact_name);
    }

    public void setData(Contact contact) {
        contactName.setText(contact.name);
    }
}
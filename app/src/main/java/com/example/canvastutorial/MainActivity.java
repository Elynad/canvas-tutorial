package com.example.canvastutorial;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = "MainActivity";

    public final static int BASE_SIZE = 250 ;
    public final static int GALLERY_REQUEST = 9 ;

    Button minusButton ;
    Button plusButton ;
    Button getBitmapButton ;
    Button shareButton ;
    TextView sizeTextView ;
    TextView statusTextView ;
    ImageView imageView ;
    ContactBitmapGenerator contactBitmapGenerator = new ContactBitmapGenerator() ;
    ImageView testImage ;
    RecyclerView contactsRv ;
    Button addContactButton ;
    Button removeContactButton ;

    SharingShortcutsManager sharingShortcutsManager = new SharingShortcutsManager();
    ContactsAdapter contactsAdapter ;

    int size = BASE_SIZE;

    ArrayList<Contact> contacts = new ArrayList<>();
    Uri lastImagePicked ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("MainActivity", "onCreate");


        minusButton = findViewById(R.id.minus_button);
        plusButton = findViewById(R.id.plus_button);
        getBitmapButton = findViewById(R.id.get_bitmap_button);
        shareButton = findViewById(R.id.share_button);
        sizeTextView = findViewById(R.id.size);
        statusTextView = findViewById(R.id.status_text);
        testImage = findViewById(R.id.test_image);
        contactsRv = findViewById(R.id.contacts_rv);
        addContactButton = findViewById(R.id.add_contact_button);
        removeContactButton = findViewById(R.id.remove_contact_button);
//        customView = new CustomView(this);

        AlertDialog.Builder alertDialogBuilderTest = new AlertDialog.Builder(this);
        alertDialogBuilderTest.setView(R.layout.avatar_layout);
        AlertDialog alertDialog = alertDialogBuilderTest.create();
        alertDialog.create();
        imageView = alertDialog.findViewById(R.id.avatar_layout);

//        customView = findViewById(R.id.custom_view);
//        customView = new CustomView(this);

        refreshSizeText();
        refreshCanvas();

        minusButton.setOnClickListener(v -> {
            if (size > 10) {
                size -= 10;
                refreshSizeText();
                refreshCanvas();
            }
        });
        plusButton.setOnClickListener(v -> {
            size += 10;
            refreshSizeText();
            refreshCanvas();
        });
        getBitmapButton.setOnClickListener(v -> {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            View dialogView = getLayoutInflater().inflate(R.layout.dialog, null);
            alertDialogBuilder.setView(dialogView);
//            customView.setDrawingCacheEnabled(true);
//            customView.measure(
//                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
//                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
//            );

            Bitmap bitmap = getBitmap();

            Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, size * 2, size * 2);
            ImageView imageView = dialogView.findViewById(R.id.image);
            imageView.setImageBitmap(resizedBitmap);
            alertDialogBuilder.create().show();
            Log.d(TAG, "Should get bitmap");
        });
        shareButton.setOnClickListener(v -> {
//            customView.invalidate();
//            Bitmap bitmap = customView.getDrawingCache(true);
            Bitmap bitmap = getBitmap();
            Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, size * 2, size * 2);
            sharingShortcutsManager.pushDirectShareTargets(this, resizedBitmap);

            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            startActivity(Intent.createChooser(sharingIntent, null));
        });

        addContactButton.setOnClickListener(v -> openNewContactDialog());

        removeContactButton.setOnClickListener(v -> removeLastContact());

        contactsAdapter = new ContactsAdapter() ;

        contactsRv.setLayoutManager(new LinearLayoutManager(this));
        contactsRv.setAdapter(contactsAdapter);
    }

    private void removeLastContact() {
        if (contacts.size() > 0) {
            contacts.remove(contacts.size() - 1);
        }
        if (contactsAdapter.getItemCount() > 0) {
            contactsAdapter.removeLastItem();
        }
        if (contactBitmapGenerator.contacts.size() > 0) {
            contactBitmapGenerator.removeLastItem();
        }
    }

    private void openNewContactDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setView(LayoutInflater.from(this).inflate(R.layout.new_contact_dialog, null));

        dialog.create();

        EditText contactNameEditText = dialog.findViewById(R.id.contact_name);
        Button chooseImageButton = dialog.findViewById(R.id.choose_image);
        Button addContactButton = dialog.findViewById(R.id.dialog_add_contact);

        chooseImageButton.setOnClickListener(v -> openGallery());

        addContactButton.setOnClickListener(v -> {
            Bitmap bitmap = null ;
            if (lastImagePicked != null) {
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), lastImagePicked);
                    String updatedStatus = "Adding new contact "
                            + contactNameEditText.getText().toString()
                            + " with image " + lastImagePicked.toString();
                    statusTextView.setText(updatedStatus);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                lastImagePicked = null;
            } else {
                String updatedStatus = "Adding new contact "
                        + contactNameEditText.getText().toString()
                        + " without image";
                statusTextView.setText(updatedStatus);
            }
            Contact contact = new Contact(
                    contactNameEditText.getText().toString(),
                    bitmap
            );
            Log.d(TAG, "Adding item " + contact.name);
            contactsAdapter.addItem(contact);
            contactBitmapGenerator.addContact(contact);

            dialog.dismiss();
        });

        dialog.show();
    }

    private void openGallery() {
        Intent galleryIntent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.INTERNAL_CONTENT_URI
        );
        startActivityForResult(galleryIntent, GALLERY_REQUEST);
    }

    private void refreshSizeText() {
        String sizeText = Integer.toString(size / 10);
        sizeTextView.setText(sizeText);
    }

    private void refreshCanvas() {
        //customView.draw(size);
        String updatedStatus = "Should update canvas with new size " + size / 10 ;
        statusTextView.setText(updatedStatus);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == GALLERY_REQUEST) {
            if (data != null) {
                lastImagePicked = data.getData();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public Bitmap getBitmap() {
        Bitmap b = Bitmap.createBitmap(size * 2, size * 2, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        contactBitmapGenerator.drawOnCanvas(c);
        return b;
    }
}
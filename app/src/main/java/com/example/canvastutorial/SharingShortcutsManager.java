package com.example.canvastutorial;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;

import androidx.core.app.Person;
import androidx.core.content.pm.ShortcutInfoCompat;
import androidx.core.content.pm.ShortcutManagerCompat;
import androidx.core.graphics.drawable.IconCompat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

public class SharingShortcutsManager {

    String categoryTextShareTarget = "com.example.canvastutorial.category.TEXT_SHARE_TARGET";

    private static final int MAX_SHORTCUTS = 4;


    public void pushDirectShareTargets(Context context, Bitmap icon) {
        ArrayList<ShortcutInfoCompat> shortcuts = new ArrayList<>();

        Intent staticLauncherShortcutIntent = new Intent(Intent.ACTION_DEFAULT);
        staticLauncherShortcutIntent.putExtra("TEST", "TEST VALUE");

        for (int i = 0 ; i < MAX_SHORTCUTS ; i++) {
            shortcuts.add(
                    new ShortcutInfoCompat.Builder(context, Integer.toString(i))
                            .setShortLabel("Contact " + i)
                            .setIcon(IconCompat.createWithBitmap(icon))
                            .setIntent(staticLauncherShortcutIntent)
                            .setLongLived(true)
                            .setCategories(Set.of(categoryTextShareTarget))
                            .setPerson(
                                    new Person.Builder()
                                            .setName("Contact " + i)
                                            .build()
                            )
                            .build()
            );

        }

        Log.d("SharingShortcutsManager", "Pushing " + shortcuts.size() + " items");

        ShortcutManagerCompat.removeAllDynamicShortcuts(context);
        ShortcutManagerCompat.addDynamicShortcuts(context, shortcuts);

    }
}

package com.example.canvastutorial;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.ArrayList;

/**
 *  This class handles avatars for Contacts. It uses Canvas in order to get a nice and clean bitmap
 *  needed for Direct Share & Bubble features.
 *
 *  Notes:
 *  - Animated GIF images will be static when drawn on the canvas
 *
 *  Specs:
 *  - Create a rounded image containing all the avatars of the given contacts.
 *      - If there is a single contact, only returns a rounded image of the profile picture.
 *      - If there are 2 contacts, return 2 semi circles.
 *      - If there are 3 contacts, return 1 semi circle and 2 quarter circles.
 *      - If there are 4 contacts, return 4 quarter circles.
 *      - If there are more than 4 contacts, the 4th quarter circle displays the count of hidden
 *          contacts.
 *  - The drawing cache is always enabled, as we need to convert this view into a Bitmap when
 *      needed.
 *
 */
public class ContactBitmapGenerator {

    private static final String TAG = "CustomView" ;

    private static final int STROKE_WIDTH = 20;

    private static int SIZE = MainActivity.BASE_SIZE;
    private static final float SEMI_TEXT_SIZE = SIZE / 2f;
    private static final float QUARTER_TEXT_SIZE = SIZE / 3f;

    private RectF   mRectF ;

    // This [Paint] is used when we draw a single contact avatar
    private Paint   mainCircle ;

    // These [Paint]s are used when we draw an avatar for a group or 2 ([leftSemiCircle] is also
    //  used on groups of 3).
    private Paint   leftSemiCircle ;
    private Paint   rightSemiCircle ;

    // These [Paint]s are used when we draw an avatar for a group of 3 or more contacts.
    // Note : We could re-use [leftSemiCircle] & [rightSemiCircle] with different configurations,
    //  however, for clearness purposes, we created a [Paint] for each arc.
    private Paint   topLeftArc ;
    private Paint   topRightArc ;
    private Paint   botLeftArc ;
    private Paint   botRightArc ;

    // These [Paint]s are used to display the first letters of each contact.
    private Paint   topLeftText ;
    private Paint   topRightText ;
    private Paint   botLeftText ;
    private Paint   botRightText ;

    ArrayList<Contact> contacts = new ArrayList<>();

    public ContactBitmapGenerator() {
        init();
    }

    public void init() {
        Log.d(TAG, "init");

        mRectF = new RectF(
                STROKE_WIDTH / 2f,
                STROKE_WIDTH / 2f,
                (2 * SIZE) - (STROKE_WIDTH / 2f),
                (2 * SIZE) - (STROKE_WIDTH / 2f)
        );

        switch (contacts.size()) {
            case 0 :
                break;
            case 1 :
                // There is only one contact, we set a single circle
                mainCircle = initSingleContactCircle(contacts.get(0).bitmap);

                // Setting text for first letter
                if (contacts.get(0).bitmap == null)
                    topLeftText = setupNewTextPaint(SEMI_TEXT_SIZE);

                break;
            case 2 :
                // There are two contacts, we set two semi circles

                // Setting left semi circle for first contact
                leftSemiCircle = initSemiCircle(contacts.get(0).bitmap, true, Color.RED);

                // Setting right semi circle for second contact
                rightSemiCircle = initSemiCircle(contacts.get(1).bitmap, false, Color.BLUE);

                // Setting text for first letters
                if (contacts.get(0).bitmap == null)
                    topLeftText = setupNewTextPaint(SEMI_TEXT_SIZE);
                if (contacts.get(1).bitmap == null)
                    botRightText = setupNewTextPaint(SEMI_TEXT_SIZE);

                break;
            case 3:
                // There are 3 contacts, we set a semi circle and 2 quarters
                Log.d(TAG + "-init", "3 contacts -> 1 Single circle & 2 quarters");

                // Setting left semi circle for first contact
                leftSemiCircle = initSemiCircle(contacts.get(0).bitmap, true, Color.RED);

                // Setting top right quarter circle
                topRightArc = initQuarterCircle(contacts.get(1).bitmap, true,
                        false, Color.BLUE);

                // Setting bot right quarter circle ; TODO : Actually bot right
                botLeftArc = initQuarterCircle(contacts.get(2).bitmap, false,
                        false, Color.GREEN);

                // Setting texts for first letters
                if (contacts.get(0).bitmap == null)
                    topLeftText = setupNewTextPaint(SEMI_TEXT_SIZE);
                if (contacts.get(1).bitmap == null)
                    topRightText = setupNewTextPaint(QUARTER_TEXT_SIZE);
                if (contacts.get(2).bitmap == null)
                    botRightText = setupNewTextPaint(QUARTER_TEXT_SIZE);

                break;
            default:
                Log.d(TAG + "-init", "4 contacts -> 4 quarters");

                // Setting top left quarter circle
                topLeftArc = initQuarterCircle(contacts.get(0).bitmap, true,
                        true, Color.RED);

                // Setting top right quarter circle
                topRightArc = initQuarterCircle(contacts.get(1).bitmap, true,
                        false, Color.BLUE);

                // Setting bot left quarter circle ;
                botLeftArc = initQuarterCircle(contacts.get(2).bitmap, false,
                        true, Color.YELLOW);

                Bitmap originalBitmap = null ;

                // If we have 4 contacts or more, gets the bitmap that will be displayed on bot
                //  right arc with opacity.
                // I.E., if contacts[3] and contacts[4] have no image but contacts[5] does, the
                // image displayed with opacity will be the image from contacts[5].
                if (contacts.size() >= 4) {
                    for (int i = 3 ; i < contacts.size() ; i++) {
                        if (contacts.get(i).bitmap != null) {
                            originalBitmap = contacts.get(i).bitmap;
                            break;
                        }
                    }
                }

                // Setting bot right quarter circle ;
                botRightArc = initQuarterCircle(originalBitmap, false,
                        false, Color.GREEN);

                // Setting some opacity if we have more than 4 contacts
                botRightArc.setAlpha((contacts.size() > 4) ? 125 : 255);

                topLeftText = setupNewTextPaint(QUARTER_TEXT_SIZE);
                topRightText = setupNewTextPaint(QUARTER_TEXT_SIZE);
                botLeftText = setupNewTextPaint(QUARTER_TEXT_SIZE);
                botRightText = setupNewTextPaint(QUARTER_TEXT_SIZE);

                break;
        }
    }

    /**
     *  This function is used when we are drawing a single contact avatar. Will create and configure
     *  a new [Paint] and return it.
     *  @param bitmap A [@Nullable] [Bitmap], used as contact profile picture.
     *  @return a configured and ready to draw [Paint].
     */
    private Paint initSingleContactCircle(@Nullable Bitmap bitmap) {

        Paint mainCirclePaint = new Paint();
        mainCirclePaint.setStyle(Paint.Style.FILL);

        if (bitmap != null) {
            Bitmap resizedBitmap = getResizedBitmap(bitmap);
            Shader bitmapShader = new BitmapShader(
                    resizedBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP
            );
            mainCirclePaint.setShader(bitmapShader);
            Matrix matrix = new Matrix();
            RectF src = new RectF(
                    0,
                    0,
                    resizedBitmap.getWidth() + 1 - 1,
                    resizedBitmap.getHeight() + 1 - 1
            );
            RectF dst = new RectF(
                    -resizedBitmap.getWidth() + SIZE,
                    -resizedBitmap.getHeight() + SIZE,
                    resizedBitmap.getWidth() + SIZE,
                    resizedBitmap.getHeight() + SIZE
            );
            matrix.setRectToRect(src, dst, Matrix.ScaleToFit.CENTER);
            bitmapShader.setLocalMatrix(matrix);
        } else {
            mainCirclePaint.setColor(Color.RED);
        }

        return mainCirclePaint;
    }

    /**
     *  This function is used to configure a semi circle, we use it when we need an avatar for 2 or
     *  3 contacts. It will create and configure a new [Paint] and return it.
     *  @param bitmap A [@Nullable] [Bitmap], used as contact profile picture.
     *  @param isLeft A [boolean] to know how to scale the image, [true] if we need a left semi
     *                circle, and [false] if we need a right semi circle.
     *  @param backupColor An [int] that set a color if the [bitmap] is null.
     *  @return a configured and ready to draw [Paint].
     */
    private Paint initSemiCircle(@Nullable Bitmap bitmap, boolean isLeft, int backupColor) {

        Paint result = new Paint();
        result.setStyle(Paint.Style.FILL);

        if (bitmap != null) {
            // TODO : Do not call for getResizedBitmap, but resize only once
            Bitmap resizedBitmap = getResizedBitmap(bitmap);
            resizedBitmap = Bitmap.createScaledBitmap(
                    resizedBitmap,
                    (int) (bitmap.getWidth() / 2.75),
                    (int) (bitmap.getHeight() / 2.75),
                    false
            );
            Shader bitmapShader = new BitmapShader(
                    resizedBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP
            );

            result.setShader(bitmapShader);
            Matrix matrix = new Matrix();
            RectF src = new RectF(
                    0,
                    0,
                    resizedBitmap.getWidth() + 1 - 1,
                    resizedBitmap.getHeight() + 1 - 1
            );
            RectF dst = new RectF(
                    /*left*/ (isLeft)
                        ? -resizedBitmap.getWidth() + SIZE / 2f
                        :  SIZE / 2f,
                    -SIZE / 2f,
                    /*right*/ (isLeft)
                        ? resizedBitmap.getWidth() + SIZE / 2f
                        : resizedBitmap.getWidth() + SIZE * 2f,
                    SIZE * 2f
            );
            matrix.setRectToRect(src, dst, Matrix.ScaleToFit.FILL);
            bitmapShader.setLocalMatrix(matrix);
        } else {
            result.setColor(backupColor);
        }

        return result;
    }


    /**
     *  This function is used to configure a quarter circle. It will create and configure a new
     *  [Paint] and return it.
     *  @param bitmap A [@Nullable] [Bitmap], used as contact profile picture.
     *  @param isTop A [boolean] to know how to scale the image, [true] if we need a top quarter
     *               circle, and [false] if we need a bottom quarter circle.
     *  @param isLeft A [boolean] to know how to scale the image, [true] if we need a left quarter
     *               circle, and [true] if we need a right quarter circle.
     *  @param backupColor An [int] that set a color if the [bitmap] is null.
     *  @return a configured and ready to draw [Paint].
     *
     * Note:
     *  - If we need a bot and right quarter circle, it looks like a special case to scale. We need
     *  a dedicated 'if' statement for this purpose.
     */
    private Paint initQuarterCircle(@Nullable Bitmap bitmap, boolean isTop, boolean isLeft,
                                    int backupColor) {
        Paint result = new Paint();
        result.setStyle(Paint.Style.FILL);
        result.setStrokeWidth(STROKE_WIDTH);
        if (bitmap != null) {
            result = new Paint(Paint.ANTI_ALIAS_FLAG);
            Bitmap resizedBitmap = getResizedBitmap(bitmap);
            Shader shader = new BitmapShader(resizedBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            Matrix matrix = new Matrix();
            RectF src = new RectF(
                    0,
                    0,
                    resizedBitmap.getWidth() + 1 - 1,
                    resizedBitmap.getHeight() + 1 - 1
            );

            RectF dst ;

            if (!isLeft && !isTop)
                // If we are setting bot right quarter, we need to handle its special case :
                dst = new RectF(
                        -resizedBitmap.getWidth() + (SIZE * 1.95f),
                        -resizedBitmap.getHeight() + (SIZE * 1.95f),
                        resizedBitmap.getWidth() + (SIZE / 1.15f),
                        resizedBitmap.getHeight() + (SIZE / 1.15f)
                );
            else
                dst = new RectF(
                        -resizedBitmap.getWidth() + SIZE,
                        -resizedBitmap.getHeight() + SIZE,
                        (isLeft) ? resizedBitmap.getWidth() : resizedBitmap.getWidth() + (SIZE * 1.75f),
                        (isTop) ? resizedBitmap.getHeight() : resizedBitmap.getHeight() + (SIZE * 1.75f)
                );

            matrix.setRectToRect(src, dst, Matrix.ScaleToFit.CENTER);
            shader.setLocalMatrix(matrix);
            result.setShader(shader);
        } else {
            result.setColor(backupColor);
        }
        return result;
    }

    protected void drawOnCanvas(Canvas canvas) {
        Log.d(TAG, "onDraw");
        init();

        // Should clear canvas
        canvas.drawColor(Color.TRANSPARENT);

        switch (contacts.size()) {
            case 0 :
                break;
            case 1 :
                Log.d(TAG + "-draw", "Single contact -> Single circle");

                // Draw background
                canvas.drawCircle(SIZE, SIZE, 200, mainCircle);

                // Draw first letter
                if (contacts.get(0).bitmap == null)
                    canvas.drawText(getFirstLetter(contacts.get(0)), SIZE - (STROKE_WIDTH * 2),
                            SIZE + STROKE_WIDTH, topLeftText);

                break;
            case 2 :
                Log.d(TAG + "-draw", "2 contacts -> 2 semi-circles");

                // Draw backgrounds
                canvas.drawArc(mRectF, 90, 180, true, leftSemiCircle);
                canvas.drawArc(mRectF, 270, 180, true, rightSemiCircle);

                // Draw first letters
                if (contacts.get(0).bitmap == null)
                    canvas.drawText(getFirstLetter(contacts.get(0)), SIZE / 2f,
                            SIZE + SIZE / 6f, topLeftText);
                if (contacts.get(1).bitmap == null)
                    canvas.drawText(getFirstLetter(contacts.get(1)), SIZE * 1.25f,
                            SIZE + SIZE / 6f, botRightText);

                break;
            case 3:
                Log.d(TAG + "-draw", "3 contacts -> 1 semi circle & 2 quarters");

                // Draw backgrounds
                canvas.drawArc(mRectF, 90, 180, true, leftSemiCircle);
                canvas.drawArc(mRectF, 270, 90, true, topRightArc);
                canvas.drawArc(mRectF, 0, 90, true, botLeftArc);

                // Draw first letters
                if (contacts.get(0).bitmap == null)
                    canvas.drawText(getFirstLetter(contacts.get(0)), SEMI_TEXT_SIZE / 1.25f,
                            SIZE + SIZE / 6f, topLeftText);
                if (contacts.get(1).bitmap == null)
                    canvas.drawText(getFirstLetter(contacts.get(1)), SIZE * 1.25f,
                            SIZE / 1.5f, topRightText);
                if (contacts.get(2).bitmap == null)
                    canvas.drawText(getFirstLetter(contacts.get(2)), SIZE * 1.25f,
                            SIZE * 1.5f, botRightText);
                break;
            default:
                Log.d(TAG + "-draw", "4 contacts -> 4 quarters");

                // Draw backgrounds
                canvas.drawArc(mRectF, 180, 90, true, topLeftArc);
                canvas.drawArc(mRectF, 270, 90, true, topRightArc);
                canvas.drawArc(mRectF, 0, 90, true, botRightArc);
                canvas.drawArc(mRectF, 90, 90, true, botLeftArc);

                // Draw first letters
                if (contacts.get(0).bitmap == null)
                    canvas.drawText(getFirstLetter(contacts.get(0)), SIZE / 2f,
                            SIZE / 1.5f, topLeftText);
                if (contacts.get(1).bitmap == null)
                    canvas.drawText(getFirstLetter(contacts.get(1)), SIZE * 1.25f,
                            SIZE / 1.5f, topRightText);
                if (contacts.get(2).bitmap == null)
                    canvas.drawText(getFirstLetter(contacts.get(2)), SIZE / 2f,
                            SIZE * 1.5f, botLeftText);
                if (contacts.size() > 4)
                    canvas.drawText("+ " + (contacts.size() - 3), SIZE * 1.15f,
                            SIZE * 1.5f, botRightText);
                else if (contacts.get(3).bitmap == null)
                    canvas.drawText(getFirstLetter(contacts.get(3)), SIZE * 1.25f,
                            SIZE * 1.5f, botRightText);

                break;
        }
    }



    /**
     *  Creates a 'standardized' sized bitmap
     *  @param source - [Bitmap] we need to resize
     *  @return A resized [Bitmap]
     */
    private Bitmap getResizedBitmap(Bitmap source) {
        return Bitmap.createScaledBitmap(
                source,
                SIZE,
                SIZE,
                false
        );
    }

    /**
     *  Creates a Paint configured for Text purposes.
     *  @return a new [Paint].
     */
    private Paint setupNewTextPaint(float textSize) {
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setTextSize(textSize);
        return paint;
    }

    private String getFirstLetter(Contact contact) {

        char[] res = new char[1];
        contact.name.getChars(0, 1, res, 0);
        return (("" + res[0]).toUpperCase());
    }

    public void draw(int size) {
        Log.d(TAG, "Refresh with size " + size);
        SIZE = size;
    }

    public void addContact(Contact contact) {
        Log.d(TAG, "Adding contact " + contact.name);
        this.contacts.add(contact);
    }

    public void removeLastItem() {
        if (contacts.size() > 0) {
            contacts.remove(contacts.size() - 1);
        }
    }
}
